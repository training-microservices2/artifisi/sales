package com.artivisi.training.microservices.sales.service;

import com.artivisi.training.microservices.sales.dto.Invoice;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@FeignClient(name = "invoice", fallback = InvoiceServiceFallback.class)
public interface InvoiceService {

    @RequestMapping("/api/invoice/")
    void create(Invoice invoice);

    @RequestMapping("/api/invoice/hostinfo")
    Map<String, Object> invoiceInfo();
}
